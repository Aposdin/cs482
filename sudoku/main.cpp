/*
sudoku puzzel solver for cs482
written by Michael Mills
*/

// Header Files
#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <string>

using namespace std;
// Global Constants
int sudoku[9][9];
bool constraints[9][9][9];

// Function Initialization
void openfile( int num );
void print();
bool solve();
void insert( int loc1, int loc2, int item );
bool victory();
bool findsingle( int &loc1, int &loc2, int &item );

void printconstraints();

// Main
int main( int argc, char **argv )
{
    for(int i=0;i<9;i++){
        for(int k=0;k<9;k++) sudoku[i][k]=0;
    }
    for(int i=0;i<9;i++){
        for(int k=0;k<9;k++){
            for(int j=0;j<9;j++) constraints[i][k][j] = true;
        }
    }
    int num;
    if( argc != 2 ) num = 1;
    else num = atoi(argv[1]);
    openfile( num );
    print();
//printconstraints();
    if( solve() )
    {
        cout << "solved" << endl; 
        print();
    }
    else cout << "cannot find solution" << endl;
    return 0;
}
 
// Function Implementation

// gets the sudoku puzzel from the file
void openfile( int num )
{
    string line;
    char hold;
    int count=0;
    ifstream myfile ("puzzels");
    if( myfile.is_open())
    {
        while( getline(myfile,line) )
        {
            if( line[0] == 'G' ) count++;
            if( count == num )
            {
                cout << line << endl;
                for(int i = 0;i<9;i++)
                {
                    for(int k=0;k<9;k++)
                    {
                        myfile >> hold;
                        insert( i, k ,(hold-'0')%48 );
                    }
                }
                myfile.close();
                return;
            }
        }
        myfile.close();
    }
    else cout << "unable to open file" << endl;
    
}
void print()
{
    for(int i = 0;i<9;i++)
    {
        for(int k=0;k<9;k++)
        {
            cout << sudoku[i][k] << " ";
            if( k%3 == 2 && k != 8 ) cout << "| ";
        } 
        if( i%3 == 2 && i != 8 ) cout << endl << "- - - + - - - + - - -";
        cout << endl;
    }
    cout << endl;
}

bool solve()
{
    int row, col, num;
    // loop while checking victory conditions
    while( !victory() )
    {
        // find number constraind to one number
        if( !findsingle( row, col, num ) ) return false;
//cout << row << " " << col << " " << num << endl;
        // insert to update constraints
        insert( row, col, num );
    }
    return true;   
}

void insert( int loc1, int loc2, int item )
{
    sudoku[loc1][loc2] = item;
    if( item == 0 ) return;
    for(int i = 0;i<9;i++)
    {
        constraints[loc1][loc2][i] = false;
    }

    for(int i = 0;i<9;i++)
    {
        constraints[i][loc2][item-1] = false;
    }
    for(int i = 0;i<9;i++)
    {
        constraints[loc1][i][item-1] = false;
    }
    if( loc1 < 3 )
    {
        if( loc2 < 3 )
        {
            constraints[0][0][item-1] = false;
            constraints[0][1][item-1] = false;
            constraints[0][2][item-1] = false;
            constraints[1][0][item-1] = false;
            constraints[1][1][item-1] = false;
            constraints[1][2][item-1] = false;
            constraints[2][0][item-1] = false;
            constraints[2][1][item-1] = false;
            constraints[2][2][item-1] = false;
        }
        else if( loc2 > 5 )
        {
            constraints[0][6][item-1] = false;
            constraints[0][7][item-1] = false;
            constraints[0][8][item-1] = false;
            constraints[1][6][item-1] = false;
            constraints[1][7][item-1] = false;
            constraints[1][8][item-1] = false;
            constraints[2][6][item-1] = false;
            constraints[2][7][item-1] = false;
            constraints[2][8][item-1] = false;
        }
        else
        {
            constraints[0][3][item-1] = false;
            constraints[0][4][item-1] = false;
            constraints[0][5][item-1] = false;
            constraints[1][3][item-1] = false;
            constraints[1][4][item-1] = false;
            constraints[1][5][item-1] = false;
            constraints[2][3][item-1] = false;
            constraints[2][4][item-1] = false;
            constraints[2][5][item-1] = false;
        }
    }
    else if( loc1 > 5 )
    {
        if( loc2 < 3 )
        {
            constraints[6][0][item-1] = false;
            constraints[6][1][item-1] = false;
            constraints[6][2][item-1] = false;
            constraints[7][0][item-1] = false;
            constraints[7][1][item-1] = false;
            constraints[7][2][item-1] = false;
            constraints[8][0][item-1] = false;
            constraints[8][1][item-1] = false;
            constraints[8][2][item-1] = false;
        }
        else if( loc2 > 5 )
        {
            constraints[6][6][item-1] = false;
            constraints[6][7][item-1] = false;
            constraints[6][8][item-1] = false;
            constraints[7][6][item-1] = false;
            constraints[7][7][item-1] = false;
            constraints[7][8][item-1] = false;
            constraints[8][6][item-1] = false;
            constraints[8][7][item-1] = false;
            constraints[8][8][item-1] = false;
        }
        else
        {
            constraints[6][3][item-1] = false;
            constraints[6][4][item-1] = false;
            constraints[6][5][item-1] = false;
            constraints[7][3][item-1] = false;
            constraints[7][4][item-1] = false;
            constraints[7][5][item-1] = false;
            constraints[8][3][item-1] = false;
            constraints[8][4][item-1] = false;
            constraints[8][5][item-1] = false;
        }
    }
    else
    {
        if( loc2 < 3 )
        {
            constraints[3][0][item-1] = false;
            constraints[3][1][item-1] = false;
            constraints[3][2][item-1] = false;
            constraints[4][0][item-1] = false;
            constraints[4][1][item-1] = false;
            constraints[4][2][item-1] = false;
            constraints[5][0][item-1] = false;
            constraints[5][1][item-1] = false;
            constraints[5][2][item-1] = false;
        }
        else if( loc2 > 5 )
        {
            constraints[3][6][item-1] = false;
            constraints[3][7][item-1] = false;
            constraints[3][8][item-1] = false;
            constraints[4][6][item-1] = false;
            constraints[4][7][item-1] = false;
            constraints[4][8][item-1] = false;
            constraints[5][6][item-1] = false;
            constraints[5][7][item-1] = false;
            constraints[5][8][item-1] = false;
        }
        else
        {
            constraints[3][3][item-1] = false;
            constraints[3][4][item-1] = false;
            constraints[3][5][item-1] = false;
            constraints[4][3][item-1] = false;
            constraints[4][4][item-1] = false;
            constraints[4][5][item-1] = false;
            constraints[5][3][item-1] = false;
            constraints[5][4][item-1] = false;
            constraints[5][5][item-1] = false;
        }
    }
    constraints[loc1][loc2][item-1] = true;
}
bool victory()
{
    int sum = 0;
    for(int i = 0;i<9;i++)
    {
        for(int k=0;k<9;k++)
        {
            if( sudoku[i][k] == 0 )sum++;
        }
    }
    if( sum > 0 ) return false;
    return true;
}
bool findsingle( int &loc1, int &loc2, int &item )
{
    //printconstraints();
    int sum, num;
    for(int i = 0;i<9;i++)
    {
        for(int k=0;k<9;k++)
        {
            sum = 0;
            for(int j=0;j<9;j++)
            {
                if( constraints[i][k][j] == true )
                {
                    num = j;
                    sum++;
                }
            }

            if( sum == 1 && sudoku[i][k] == 0 )
            {
                loc1 = i;loc2 = k; item = num+1;
                return true;
            }
        }
    }
    return false;
}
void printconstraints()
{
    for(int i = 0;i<9;i++)
    {
        for(int k=0;k<9;k++)
        {
            for( int j=0;j<9;j++)
            {
                if(constraints[i][k][j] == true) cout << 1;
                else cout << 0;
            }
            cout << " ";
            if( k%3 == 2 && k != 8 ) cout << "| ";
        } 
        if( i%3 == 2 && i != 8 ) cout << endl << "- - - + - - - + - - -";
        cout << endl;
    }
    cout << endl;
}
