/*
Michael Mills
cs482 ai player connect 4

*/

// Header Files
#include <iostream>
#include <string>
#include <stdlib.h>

using namespace std;
// Global Constants

struct board
{
    char feild[6][7];

    board()
    {
        for(int i=0;i<6;i++)
        {
            for(int j=0;j<7;j++) feild[i][j] = '-';
        }
    }
    void copy( board updater );
    bool update( int selection, char symbol );
    void print();
};

struct node
{
    board state;
    bool computer;
    node* one;
    node* two;
    node* three;
    node* four;
    node* five;
    node* six;
    node* seven;

    node(): one(NULL), two(NULL), three(NULL), four(NULL),
            five(NULL), six(NULL), seven(NULL){}
};

struct minimax
{
    node* initial;

    int Decision( node* current, int depth );
    int maxValue( node* current, int depth );
    int minValue( node* current, int depth );

};

struct abminimax
{
    node* initial;

    int abDecision( node* current, int depth );
    int abMaxValue( node* current, int alpha, int beta, int depth );
    int abMinValue( node* current, int alpha, int beta, int depth );
};

// Function Initialization
// minimax tree functions

bool Player( node* current );
void Actions( node* current );
node* Results( node* current, int a );
int Eval( node* current );
bool CutoffTest( node* current );
int max( int , int  );
int min( int , int  );

//menu game functions
void menu( bool &computer, bool &ab );
void playreg( minimax game, int depth );
void playab( abminimax game, int depth );
bool firstVictory( node* current );
bool secondVictory( node* current );

// Main /////////////////////////////////////////////////////////////////////
int main( int argc, char **argv )
{
    int depth;
    if( argc != 2 ) depth = 6;
    else depth = atoi(argv[1]);
    bool ab;
    bool computer;

    menu( computer, ab );

    if( ab )
    {
        abminimax game;
        game.initial = new node;
        game.initial->computer = computer;
        playab( game, depth );
    }else
    {
        minimax game;
        game.initial = new node;
        game.initial->computer = computer;
        playreg( game, depth );
    }

    return 0;
}
// Function Implimentation

void menu( bool &computer, bool &ab )
{
    char hold;
    char first;
    cout << endl << "Welcome to Connect Four" << endl << endl;

    // select alpha beta pruning
    cout << "Do you wish to play with ab pruning? y:n : ";
    cin >> hold;
    if( hold == 'y' || hold == 'Y' ) ab = true; else ab = false;

    // select first person player
    cout << "do you want the computer to go first? y:n : ";
    cin >> first;

    if( first == 'y' || first == 'Y' ) computer = true; 
    else computer = false;

    cout << endl << endl;
}

void playreg( minimax game, int depth )
{
    bool finish = false, valid;
    int move;
    node* current = game.initial;

    if( current->computer )
    {
        while(!finish)
        {
            current->computer = true;
            move = game.Decision( current, depth );
            current = Results( current, move );
            current->state.print();
            finish = firstVictory( current );
            if( finish )
            {
                current->state.print();
                cout << endl << "Computer Wins!" << endl << endl;
                continue;
            }
            current->computer = false;
            do
            {
                cout << endl << "Enter a move 1-7: ";
                cin >> move;
                if( Results(current,move) == NULL )
                {
                    valid = false;
                    cout << "Invalid Move, Please Try Again" << endl;
                }else valid = true;
            }while(!valid);
            current = Results( current, move );
            current->state.print();
            finish = secondVictory( current );
            if( finish )
            {
                current->state.print();
                cout << endl << "Player Wins!" << endl << endl;
                continue;
            }
        }
    }
    else
    {
        current->computer = true;
        Actions( current );
        current->state.print();
        while(!finish)
        {
            do
            {
                cout << endl << "Enter a move 1-7: ";
                cin >> move;
                if( Results(current,move) == NULL )
                {
                    valid = false;
                    cout << "Invalid Move, Please Try Again" << endl;
                }else valid = true;
            }while(!valid);
            current = Results( current, move );
            current->state.print();
            finish = firstVictory( current );
            if( finish )
            {
                current->state.print();
                cout << endl << "Player Wins!" << endl << endl;
                continue;
            }

            current->computer = false;
            move = game.Decision( current, depth );
            current = Results( current, move );
            current->state.print();
            finish = secondVictory( current );
            if( finish )
            {
                current->state.print();
                cout << endl << "Computer Wins!" << endl << endl;
                continue;
            }
        }
    }

}

void playab( abminimax game, int depth )
{
   bool finish = false, valid;
    int move;
    node* current = game.initial;

    if( current->computer )
    {
        while(!finish)
        {
            current->computer = true;
            move = game.abDecision( current, depth );
            current = Results( current, move );
            current->state.print();
            finish = firstVictory( current );
            if( finish )
            {
                current->state.print();
                cout << endl << "Computer Wins!" << endl << endl;
                continue;
            }
            current->computer = false;
            do
            {
                cout << endl << "Enter a move 1-7: ";
                cin >> move;
                if( Results(current,move) == NULL )
                {
                    valid = false;
                    cout << "Invalid Move, Please Try Again" << endl;
                }else valid = true;
            }while(!valid);
            current = Results( current, move );
            current->state.print();
            finish = secondVictory( current );
            if( finish )
            {
                current->state.print();
                cout << endl << "Player Wins!" << endl << endl;
                continue;
            }
        }
    }
    else
    {
        current->computer = true;
        Actions( current );
        current->state.print();
        while(!finish)
        {
            do
            {
                cout << endl << "Enter a move 1-7: ";
                cin >> move;
                if( Results(current,move) == NULL )
                {
                    valid = false;
                    cout << "Invalid Move, Please Try Again" << endl;
                }else valid = true;
            }while(!valid);
            current = Results( current, move );
            current->state.print();
            finish = firstVictory( current );
            if( finish )
            {
                current->state.print();
                cout << endl << "Player Wins!" << endl << endl;
                continue;
            }

            current->computer = false;
            move = game.abDecision( current, depth );
            current = Results( current, move );
            current->state.print();
            finish = secondVictory( current );
            if( finish )
            {
                current->state.print();
                cout << endl << "Computer Wins!" << endl << endl;
                continue;
            }
        }
    }
}

void board::copy( board updater )
{
    for(int i=0;i<6;i++)
    {
        for(int j=0;j<7;j++) feild[i][j] = updater.feild[i][j];
    }
}
bool board::update( int selection, char symbol )
{
    if( selection != 1 && selection != 2 && selection != 3 &&
        selection != 4 && selection != 5 && selection != 6 &&
        selection != 7 ) return false;

    for( int i=5; i>=0; i-- )
    {
        if( feild[i][selection-1] == '-' )
        {
            feild[i][selection-1] = symbol;
            return true;
        }    
    }
    return false;
}

void board::print()
{

    cout << endl;
     for(int i=0;i<6;i++)
    {
        for(int j=0;j<7;j++)
        {
            cout << "  " << feild[i][j] << " ";
        }
     cout << endl;
    }
    cout << "  " << 1 << " ";
    cout << "  " << 2 << " ";
    cout << "  " << 3 << " ";
    cout << "  " << 4 << " ";
    cout << "  " << 5 << " ";
    cout << "  " << 6 << " ";
    cout << "  " << 7 << " ";
    cout << endl << endl;
}

bool Player( node* current )
{
    return current->computer;
}
void Actions( node* current )
{
    char c;
    if( current->computer ) c = 'x'; else c = 'o';
    bool valid;
    // creates nodes for each possible move
    // move at one
    if( current->one == NULL )
    {
        current->one = new node;
        current->one->state.copy( current->state );
        valid = current->one->state.update(1,c);
        current->one->computer = current->computer;
        if( !valid )
        {
            delete current->one;
            current->one = NULL;
        }
    }
    // move at two
    if( current->two == NULL )
    {
        current->two = new node;
        current->two->state.copy( current->state );
        valid = current->two->state.update(2,c);
        current->two->computer = current->computer;
        if( !valid )
        {
            delete current->two;
            current->two = NULL;
        }
    }
    // move at three
    if( current->three == NULL )
    {
        current->three = new node;
        current->three->state.copy( current->state );
        valid = current->three->state.update(3,c);
        current->three->computer = current->computer;
        if( !valid )
        {
            delete current->three;
            current->three = NULL;
        }
    }
    // move at four
    if( current->four == NULL )
    {
        current->four = new node;
        current->four->state.copy( current->state );
        valid = current->four->state.update(4,c);
        current->four->computer = current->computer;
        if( !valid )
        {
            delete current->four;
            current->four = NULL;
        }
    } 
    // move at five
    if( current->five == NULL )
    {
        current->five = new node;
        current->five->state.copy( current->state );
        valid = current->five->state.update(5,c);
        current->five->computer = current->computer;
        if( !valid )
        {
            delete current->five;
            current->five = NULL;
        }
    }
    // move at six
    if( current->six == NULL )
    {
        current->six = new node;
        current->six->state.copy( current->state );
        valid = current->six->state.update(6,c);
        current->six->computer = current->computer;
        if( !valid )
        {
            delete current->six;
            current->six = NULL;
        }
    }
    // move at seven
    if( current->seven == NULL )
    {
        current->seven = new node;
        current->seven->state.copy( current->state );
        valid = current->seven->state.update(7,c);
        current->seven->computer = current->computer;
        if( !valid )
        {
            delete current->seven;
            current->seven = NULL;
        }
    }
}
node* Results( node* current, int a )
{
    if( a == 1 ) return current->one;
    else if( a == 2 ) return current->two;
    else if( a == 3 ) return current->three;
    else if( a == 4 ) return current->four;
    else if( a == 5 ) return current->five;
    else if( a == 6 ) return current->six;
    else if( a == 7 ) return current->seven;
}
int Eval( node* current )
{
    int first,second,score = 0;

    if( firstVictory( current ) ) score += 60;
    if( secondVictory( current ) ) score -= 60;

    // search rows
    for(int i=0;i<6;i++)
    {
        for(int j=0;j<4;j++)
        {
            first = second = 0;
            for(int k=0;k<4;k++)
            {
                if( current->state.feild[i][j+k] == 'x' ) first++;
                else if( current->state.feild[i][j+k] == 'o' ) second++;
            }
            if( !current->computer )
            {
                if( first == 0 && second != 0 ) score -= second;
                if( second == 0 && first != 0 ) score += first*first;
            }else
            {
                if( first == 0 && second != 0 ) score -= second*second;
                if( second == 0 && first != 0 ) score += first;
            
            }
        }
    }
    // search columns
    for(int i=0;i<7;i++)
    {
        for(int j=0;j<3;j++)
        {
            first = second = 0;
            for(int k=0;k<4;k++)
            {
                if( current->state.feild[j+k][i] == 'x' ) first++;
                else if( current->state.feild[j+k][i] == 'o' ) second++;
            }
            if( !current->computer )
            {
                if( first == 0 && second != 0 ) score -= second;
                if( second == 0 && first != 0 ) score += first*first;
            }else
            {
                if( first == 0 && second != 0 ) score -= second*second;
                if( second == 0 && first != 0 ) score += first;
            
            }
        }
    }
    // search diagnals
    for(int i=0;i<3;i++)
    {
        for(int j=0;j<2;j++)
        {
            first = second = 0;
            for(int k=0;k<4;k++)
            {
                if( current->state.feild[i+k][i+j+k] == 'x' ) first++;
                else if( current->state.feild[i+k][i+j+k] == 'o' ) second++;
            }
            if( !current->computer )
            {
                if( first == 0 && second != 0 ) score -= second;
                if( second == 0 && first != 0 ) score += first*first;
            }else
            {
                if( first == 0 && second != 0 ) score -= second*second;
                if( second == 0 && first != 0 ) score += first;
            
            }
        }
    }
    for(int i=0;i<3;i++)
    {
        for(int j=0;j<2;j++)
        {
            first = second = 0;
            for(int k=0;k<4;k++)
            {
                if( current->state.feild[5-i+k][i+j+k] == 'x' ) first++;
                else if( current->state.feild[5-i+k][i+j+k] == 'o' ) second++;
            }
            if( !current->computer )
            {
                if( first == 0 && second != 0 ) score -= second;
                if( second == 0 && first != 0 ) score += first*first;
            }else
            {
                if( first == 0 && second != 0 ) score -= second*second;
                if( second == 0 && first != 0 ) score += first;
            
            }
        }
    }
    for(int i=0;i<2;i++)
    {
        first = second = 0;
        for(int k=0;k<4;k++)
        {
            if( current->state.feild[i+1+k][i+k] == 'x' ) first++;
            else if( current->state.feild[1+i+k][i+k] == 'o' ) second++;
        }
            if( !current->computer )
            {
                if( first == 0 && second != 0 ) score -= second;
                if( second == 0 && first != 0 ) score += first*first;
            }else
            {
                if( first == 0 && second != 0 ) score -= second*second;
                if( second == 0 && first != 0 ) score += first;
            
            }
    }
    for(int i=0;i<2;i++)
    {
        first = second = 0;
        for(int k=0;k<4;k++)
        {
            if( current->state.feild[i+k][i+k+2] == 'x' ) first++;
            else if( current->state.feild[i+k][i+k+2] == 'o' ) second++;
        }
            if( !current->computer )
            {
                if( first == 0 && second != 0 ) score -= second;
                if( second == 0 && first != 0 ) score += first*first;
            }else
            {
                if( first == 0 && second != 0 ) score -= second*second;
                if( second == 0 && first != 0 ) score += first;
            
            }
    }
    for(int i=0;i<2;i++)
    {
        first = second = 0;
        for(int k=0;k<4;k++)
        {
            if( current->state.feild[4-i-k][i+k] == 'x' ) first++;
            else if( current->state.feild[4-i-k][i+k] == 'o' ) second++;
        }
            if( !current->computer )
            {
                if( first == 0 && second != 0 ) score -= second;
                if( second == 0 && first != 0 ) score += first*first;
            }else
            {
                if( first == 0 && second != 0 ) score -= second*second;
                if( second == 0 && first != 0 ) score += first;
            
            }
    }
    for(int i=0;i<2;i++)
    {
        first = second = 0;
        for(int k=0;k<4;k++)
        {
            if( current->state.feild[5-i-k][i+k+2] == 'x' ) first++;
            else if( current->state.feild[5-i-k][i+k+2] == 'o' ) second++;
        }
            if( !current->computer )
            {
                if( first == 0 && second != 0 ) score -= second;
                if( second == 0 && first != 0 ) score += first*first;
            }else
            {
                if( first == 0 && second != 0 ) score -= second*second;
                if( second == 0 && first != 0 ) score += first;
            
            }
    }
    first = second = 0;
    for(int k=0;k<4;k++)
    {
        if( current->state.feild[2+k][k] == 'x' ) first++;
        else if( current->state.feild[2+k][k] == 'o' ) second++;
    }
            if( !current->computer )
            {
                if( first == 0 && second != 0 ) score -= second;
                if( second == 0 && first != 0 ) score += first*first;
            }else
            {
                if( first == 0 && second != 0 ) score -= second*second;
                if( second == 0 && first != 0 ) score += first;
            
            }
    first = second = 0;
    for(int k=0;k<4;k++)
    {
        if( current->state.feild[k][3+k] == 'x' ) first++;
        else if( current->state.feild[k][3+k] == 'o' ) second++;
    }
            if( !current->computer )
            {
                if( first == 0 && second != 0 ) score -= second;
                if( second == 0 && first != 0 ) score += first*first;
            }else
            {
                if( first == 0 && second != 0 ) score -= second*second;
                if( second == 0 && first != 0 ) score += first;
            
            }
    first = second = 0;
    for(int k=0;k<4;k++)
    {
        if( current->state.feild[3-k][k] == 'x' ) first++;
        else if( current->state.feild[3-k][k] == 'o' ) second++;
    }
            if( !current->computer )
            {
                if( first == 0 && second != 0 ) score -= second;
                if( second == 0 && first != 0 ) score += first*first;
            }else
            {
                if( first == 0 && second != 0 ) score -= second*second;
                if( second == 0 && first != 0 ) score += first;
            
            }
    first = second = 0;
    for(int k=0;k<4;k++)
    {
        if( current->state.feild[5-k][3+k] == 'x' ) first++;
        else if( current->state.feild[5-k][3+k] == 'o' ) second++;
    }
            if( !current->computer )
            {
                if( first == 0 && second != 0 ) score -= second;
                if( second == 0 && first != 0 ) score += first*first;
            }else
            {
                if( first == 0 && second != 0 ) score -= second*second;
                if( second == 0 && first != 0 ) score += first;
            
            }
    

    return score;
}
bool CutoffTest( node* current )
{
    //check for victory
    if( current == NULL ) return true;
    return false;
}
int minimax::Decision( node* current, int depth )
{
    int maxVal, val, nextMove;
    Actions(current);
    if( current->computer )
    {
        maxVal = -1000;

        if( Results(current,1) != NULL )
        {
            val = maxValue( Results( current, 1 ), depth );
            if( val > maxVal )
            {
                maxVal = val;
                nextMove = 1;
cout << 1 << " first " << val << endl;
            }
        }

        if( Results(current,2) != NULL )
        {
            val = maxValue( Results( current, 2 ), depth );
            if( val > maxVal )
            {
                maxVal = val;
                nextMove = 2;
cout << 2 << " first " << val << endl;
            }
        }

        if( Results(current,3) != NULL )
        {
            val = maxValue( Results( current, 3 ), depth );
            if( val > maxVal )
            {
                maxVal = val;
                nextMove = 3;
cout << 3 << " first " << val << endl;
            }
        }

        if( Results(current,4) != NULL )
        {
            val = maxValue( Results( current, 4 ), depth );
            if( val > maxVal )
            {
                maxVal = val;
                nextMove = 4;
cout << 4 << " first " << val << endl;
            }
        }

        if( Results(current,5) != NULL )
        {
            val = maxValue( Results( current, 5 ), depth );
            if( val > maxVal )
            {
                maxVal = val;
                nextMove = 5;
cout << 5 << " first " << val << endl;
            }
        }

        if( Results(current,6) != NULL )
        {
            val = maxValue( Results( current, 6 ), depth );
            if( val > maxVal )
            {
                maxVal = val;
                nextMove = 6;
cout << 6 << " first " << val << endl;
            }
        }

        if( Results(current,7) != NULL )
        {
            val = maxValue( Results( current, 7 ), depth );
            if( val > maxVal )
            {
                maxVal = val;
                nextMove = 7;
cout << 7 << " first " << val << endl;
            }
        }

        return nextMove;
    }else
    {
        maxVal = 1000;

        if( Results(current,1) != NULL )
        {
            val = minValue( Results( current, 1 ), depth );
            if( val < maxVal )
            {
                maxVal = val;
                nextMove = 1;
cout << 1 << " second " << val << endl;
            }
        }

        if( Results(current,2) != NULL )
        {
            val = minValue( Results( current, 2 ), depth );
            if( val < maxVal )
            {
                maxVal = val;
                nextMove = 2;
cout << 2 << " second " << val << endl;
            }
        }

        if( Results(current,3) != NULL )
        {
            val = minValue( Results( current, 3 ), depth );
            if( val < maxVal )
            {
                maxVal = val;
                nextMove = 3;
cout << 3 << " second " << val << endl;
            }
        }

        if( Results(current,4) != NULL )
        {
            val = minValue( Results( current, 4 ), depth );
            if( val < maxVal )
            {
                maxVal = val;
                nextMove = 4;
cout << 4 << " second " << val << endl;
            }
        }

        if( Results(current,5) != NULL )
        {
            val = minValue( Results( current, 5 ), depth );
            if( val < maxVal )
            {
                maxVal = val;
                nextMove = 5;
cout << 5 << " second " << val << endl;
            }
        }

        if( Results(current,6) != NULL )
        {
            val = minValue( Results( current, 6 ), depth );
            if( val < maxVal )
            {
                maxVal = val;
                nextMove = 6;
cout << 6 << " second " << val << endl;
            }
        }

        if( Results(current,7) != NULL )
        {
            val = minValue( Results( current, 7 ), depth );
            if( val < maxVal )
            {
                maxVal = val;
                nextMove = 7;
cout << 7 << " second " << val << endl;
            }
        }
        
        return nextMove;
    }
}
int minimax::maxValue( node* current, int depth )
{
    if( current == NULL ) return 5000;
    if(depth ==0 || CutoffTest( current ) ) return Eval( current );
    int v = -10000;
    current->computer = !current->computer;
    Actions( current );
    v = max( v, minValue( Results( current, 1 ), depth-1));
    v = max( v, minValue( Results( current, 2 ), depth-1));
    v = max( v, minValue( Results( current, 3 ), depth-1));
    v = max( v, minValue( Results( current, 4 ), depth-1));
    v = max( v, minValue( Results( current, 5 ), depth-1));
    v = max( v, minValue( Results( current, 6 ), depth-1));
    v = max( v, minValue( Results( current, 7 ), depth-1));

    return v;
}
int minimax::minValue( node* current, int depth )
{
    if( current == NULL ) return -5000;
    if(depth == 0 || CutoffTest( current ) ) return Eval( current );
    int v = 10000;
    current->computer = !current->computer;
    Actions( current );
    v = min( v, maxValue( Results( current, 1 ), depth-1));
    v = min( v, maxValue( Results( current, 2 ), depth-1));
    v = min( v, maxValue( Results( current, 3 ), depth-1));
    v = min( v, maxValue( Results( current, 4 ), depth-1));
    v = min( v, maxValue( Results( current, 5 ), depth-1));
    v = min( v, maxValue( Results( current, 6 ), depth-1));
    v = min( v, maxValue( Results( current, 7 ), depth-1));

    return v;
}

int abminimax::abDecision( node* current, int depth )
{
    int alpha = -1000,beta = 1000;
    int maxVal, val, nextMove;
    Actions(current);
    if( current->computer )
    {
        maxVal = -1000;

        if( Results(current,1) != NULL )
        {
            val = abMaxValue( Results( current, 1 ), alpha, beta, depth );
            if( val > maxVal )
            {
                maxVal = val;
                nextMove = 1;
cout << 1 << " first " << val << endl;
            }
        }

        if( Results(current,2) != NULL )
        {
            val = abMaxValue( Results( current, 2 ), alpha, beta, depth );
            if( val > maxVal )
            {
                maxVal = val;
                nextMove = 2;
cout << 2 << " first " << val << endl;
            }
        }

        if( Results(current,3) != NULL )
        {
            val = abMaxValue( Results( current, 3 ), alpha, beta, depth );
            if( val > maxVal )
            {
                maxVal = val;
                nextMove = 3;
cout << 3 << " first " << val << endl;
            }
        }

        if( Results(current,4) != NULL )
        {
            val = abMaxValue( Results( current, 4 ), alpha, beta, depth );
            if( val > maxVal )
            {
                maxVal = val;
                nextMove = 4;
cout << 4 << " first " << val << endl;
            }
        }

        if( Results(current,5) != NULL )
        {
            val = abMaxValue( Results( current, 5 ), alpha, beta, depth );
            if( val > maxVal )
            {
                maxVal = val;
                nextMove = 5;
cout << 5 << " first " << val << endl;
            }
        }

        if( Results(current,6) != NULL )
        {
            val = abMaxValue( Results( current, 6 ), alpha, beta, depth );
            if( val > maxVal )
            {
                maxVal = val;
                nextMove = 6;
cout << 6 << " first " << val << endl;
            }
        }

        if( Results(current,7) != NULL )
        {
            val = abMaxValue( Results( current, 7 ), alpha, beta, depth );
            if( val > maxVal )
            {
                maxVal = val;
                nextMove = 7;
cout << 7 << " first " << val << endl;
            }
        }

        return nextMove;
    }else
    {
        maxVal = 1000;

        if( Results(current,1) != NULL )
        {
            val = abMinValue( Results( current, 1 ), alpha, beta, depth );
            if( val < maxVal )
            {
                maxVal = val;
                nextMove = 1;
cout << 1 << " second " << val << endl;
            }
        }

        if( Results(current,2) != NULL )
        {
            val = abMinValue( Results( current, 2 ), alpha, beta, depth );
            if( val < maxVal )
            {
                maxVal = val;
                nextMove = 2;
cout << 2 << " second " << val << endl;
            }
        }

        if( Results(current,3) != NULL )
        {
            val = abMinValue( Results( current, 3 ), alpha, beta, depth );
            if( val < maxVal )
            {
                maxVal = val;
                nextMove = 3;
cout << 3 << " second " << val << endl;
            }
        }

        if( Results(current,4) != NULL )
        {
            val = abMinValue( Results( current, 4 ), alpha, beta, depth );
            if( val < maxVal )
            {
                maxVal = val;
                nextMove = 4;
cout << 4 << " second " << val << endl;
            }
        }

        if( Results(current,5) != NULL )
        {
            val = abMinValue( Results( current, 5 ), alpha, beta, depth );
            if( val < maxVal )
            {
                maxVal = val;
                nextMove = 5;
cout << 5 << " second " << val << endl;
            }
        }

        if( Results(current,6) != NULL )
        {
            val = abMinValue( Results( current, 6 ), alpha, beta, depth );
            if( val < maxVal )
            {
                maxVal = val;
                nextMove = 6;
cout << 6 << " second " << val << endl;
            }
        }

        if( Results(current,7) != NULL )
        {
            val = abMinValue( Results( current, 7 ), alpha, beta, depth );
            if( val < maxVal )
            {
                maxVal = val;
                nextMove = 7;
cout << 7 << " second " << val << endl;
            }
        }
        
        return nextMove;
    }
}
int abminimax::abMaxValue( node* current, int alpha, int beta, int depth )
{
    if( current == NULL ) return 5000;
    if(depth ==0 || CutoffTest( current ) ) return Eval( current );
    int v = -10000;
    current->computer = !current->computer;
    Actions( current );
    v = max( v, abMinValue( Results( current, 1 ), alpha, beta, depth-1));
    if( v >= beta ) return v;
    alpha = max( alpha, v );
    v = max( v, abMinValue( Results( current, 2 ), alpha, beta, depth-1));
    if( v >= beta ) return v;
    alpha = max( alpha, v );
    v = max( v, abMinValue( Results( current, 3 ), alpha, beta, depth-1));
    if( v >= beta ) return v;
    alpha = max( alpha, v );
    v = max( v, abMinValue( Results( current, 4 ), alpha, beta, depth-1));
    if( v >= beta ) return v;
    alpha = max( alpha, v );
    v = max( v, abMinValue( Results( current, 5 ), alpha, beta, depth-1));
    if( v >= beta ) return v;
    alpha = max( alpha, v );
    v = max( v, abMinValue( Results( current, 6 ), alpha, beta, depth-1));
    if( v >= beta ) return v;
    alpha = max( alpha, v );
    v = max( v, abMinValue( Results( current, 7 ), alpha, beta, depth-1));
    if( v >= beta ) return v;
    alpha = max( alpha, v );

    return v;
}
int abminimax::abMinValue( node* current, int alpha, int beta, int depth )
{
    if( current == NULL ) return -5000;
    if(depth == 0 || CutoffTest( current ) ) return Eval( current );
    int v = 10000;
    current->computer = !current->computer;
    Actions( current );
    v = min( v, abMaxValue( Results( current, 1 ), alpha, beta, depth-1));
    if( v <= alpha ) return v;
    beta = min( beta, v );
    v = min( v, abMaxValue( Results( current, 2 ), alpha, beta, depth-1));
    if( v <= alpha ) return v;
    beta = min( beta, v );
    v = min( v, abMaxValue( Results( current, 3 ), alpha, beta, depth-1));
    if( v <= alpha ) return v;
    beta = min( beta, v );
    v = min( v, abMaxValue( Results( current, 4 ), alpha, beta, depth-1));
    if( v <= alpha ) return v;
    beta = min( beta, v );
    v = min( v, abMaxValue( Results( current, 5 ), alpha, beta, depth-1));
    if( v <= alpha ) return v;
    beta = min( beta, v );
    v = min( v, abMaxValue( Results( current, 6 ), alpha, beta, depth-1));
    if( v <= alpha ) return v;
    beta = min( beta, v );
    v = min( v, abMaxValue( Results( current, 7 ), alpha, beta, depth-1));
    if( v <= alpha ) return v;
    beta = min( beta, v );

    return v;
}

int max( int first, int second )
{
    int hold = first;
    if( hold > second ) return hold;
    else return second;
}
int min( int first, int second )
{
    int hold = first;
    if( hold < second ) return hold;
    else return second;
}


bool firstVictory( node* current )
{
    // search rows
    for(int i=0;i<6;i++)
    {
        for(int j=0;j<4;j++)
        {
            if( current->state.feild[i][j] == 'x' &&
                current->state.feild[i][j+1] == 'x' &&
                current->state.feild[i][j+2] == 'x' &&
                current->state.feild[i][j+3] == 'x' )
            {
                return true;
            }
        }
    }
    // search columns
    for(int i=0;i<7;i++)
    {
        for(int j=0;j<3;j++)
        {
            if( current->state.feild[j][i] == 'x' &&
                current->state.feild[j+1][i] == 'x' &&
                current->state.feild[j+2][i] == 'x' &&
                current->state.feild[j+3][i] == 'x' )
            {
                return true;
            }
        }
    }
    // search diagonals
    for(int i=0;i<3;i++)
    {
        for(int j=0;j<2;j++)
        {
            if( current->state.feild[0+i][0+i+j] == 'x' &&
                current->state.feild[1+i][1+i+j] == 'x' &&
                current->state.feild[2+i][2+i+j] == 'x' &&
                current->state.feild[3+i][3+i+j] == 'x' )
            {
                return true;
            }
        }
    }
    for(int i=0;i<2;i++)
    {
        if( current->state.feild[1+i][0+i] == 'x' &&
            current->state.feild[2+i][1+i] == 'x' &&
            current->state.feild[3+i][2+i] == 'x' &&
            current->state.feild[4+i][3+i] == 'x' )
        {
            return true;
        }
    }
    for(int i=0;i<2;i++)
    {
        if( current->state.feild[0+i][2+i] == 'x' &&
            current->state.feild[1+i][3+i] == 'x' &&
            current->state.feild[2+i][4+i] == 'x' &&
            current->state.feild[3+i][5+i] == 'x' )
        {
            return true;
        }
    }
    if( current->state.feild[2][0] == 'x' &&
        current->state.feild[3][1] == 'x' &&
        current->state.feild[4][2] == 'x' &&
        current->state.feild[5][3] == 'x' )
    {
        return true;
    }
    if( current->state.feild[0][3] == 'x' &&
        current->state.feild[1][4] == 'x' &&
        current->state.feild[2][5] == 'x' &&
        current->state.feild[3][6] == 'x' )
    {
        return true;
    }
    // diagonals in other direction
    for(int i=0;i<3;i++)
    {
        for(int j=0;j<2;j++)
        {
            if( current->state.feild[5-i][0+i+j] == 'x' &&
                current->state.feild[4-i][1+i+j] == 'x' &&
                current->state.feild[3-i][2+i+j] == 'x' &&
                current->state.feild[2-i][3+i+j] == 'x' )
            {
                return true;
            }
        }
    }
    for(int i=0;i<2;i++)
    {
        if( current->state.feild[4-i][0+i] == 'x' &&
            current->state.feild[3-i][1+i] == 'x' &&
            current->state.feild[2-i][2+i] == 'x' &&
            current->state.feild[1-i][3+i] == 'x' )
        {
            return true;
        }
    }
    for(int i=0;i<2;i++)
    {
        if( current->state.feild[5-i][2+i] == 'x' &&
            current->state.feild[4-i][3+i] == 'x' &&
            current->state.feild[3-i][4+i] == 'x' &&
            current->state.feild[2-i][5+i] == 'x' )
        {
            return true;
        }
    }
    if( current->state.feild[3][0] == 'x' &&
        current->state.feild[2][1] == 'x' &&
        current->state.feild[1][2] == 'x' &&
        current->state.feild[0][3] == 'x' )
    {
        return true;
    }
    if( current->state.feild[5][3] == 'x' &&
        current->state.feild[4][4] == 'x' &&
        current->state.feild[3][5] == 'x' &&
        current->state.feild[2][6] == 'x' )
    {
        return true;
    }
    return false;
}
bool secondVictory( node* current )
{
    // search rows
    for(int i=0;i<6;i++)
    {
        for(int j=0;j<4;j++)
        {
            if( current->state.feild[i][j] == 'o' &&
                current->state.feild[i][j+1] == 'o' &&
                current->state.feild[i][j+2] == 'o' &&
                current->state.feild[i][j+3] == 'o' )
            {
                return true;
            }
        }
    }
    // search columns
    for(int i=0;i<7;i++)
    {
        for(int j=0;j<3;j++)
        {
            if( current->state.feild[j][i] == 'o' &&
                current->state.feild[j+1][i] == 'o' &&
                current->state.feild[j+2][i] == 'o' &&
                current->state.feild[j+3][i] == 'o' )
            {
                return true;
            }
        }
    }
    // search diagonals
    for(int i=0;i<3;i++)
    {
        for(int j=0;j<2;j++)
        {
            if( current->state.feild[0+i][0+i+j] == 'o' &&
                current->state.feild[1+i][1+i+j] == 'o' &&
                current->state.feild[2+i][2+i+j] == 'o' &&
                current->state.feild[3+i][3+i+j] == 'o' )
            {
                return true;
            }
        }
    }
    for(int i=0;i<2;i++)
    {
        if( current->state.feild[1+i][0+i] == 'o' &&
            current->state.feild[2+i][1+i] == 'o' &&
            current->state.feild[3+i][2+i] == 'o' &&
            current->state.feild[4+i][3+i] == 'o' )
        {
            return true;
        }
    }
    for(int i=0;i<2;i++)
    {
        if( current->state.feild[0+i][2+i] == 'o' &&
            current->state.feild[1+i][3+i] == 'o' &&
            current->state.feild[2+i][4+i] == 'o' &&
            current->state.feild[3+i][5+i] == 'o' )
        {
            return true;
        }
    }
    if( current->state.feild[2][0] == 'o' &&
        current->state.feild[3][1] == 'o' &&
        current->state.feild[4][2] == 'o' &&
        current->state.feild[5][3] == 'o' )
    {
        return true;
    }
    if( current->state.feild[0][3] == 'o' &&
        current->state.feild[1][4] == 'o' &&
        current->state.feild[2][5] == 'o' &&
        current->state.feild[3][6] == 'o' )
    {
        return true;
    }
    // diagonals in other direction
    for(int i=0;i<3;i++)
    {
        for(int j=0;j<2;j++)
        {
            if( current->state.feild[5-i][0+i+j] == 'o' &&
                current->state.feild[4-i][1+i+j] == 'o' &&
                current->state.feild[3-i][2+i+j] == 'o' &&
                current->state.feild[2-i][3+i+j] == 'o' )
            {
                return true;
            }
        }
    }
    for(int i=0;i<2;i++)
    {
        if( current->state.feild[4-i][0+i] == 'o' &&
            current->state.feild[3-i][1+i] == 'o' &&
            current->state.feild[2-i][2+i] == 'o' &&
            current->state.feild[1-i][3+i] == 'o' )
        {
            return true;
        }
    }
    for(int i=0;i<2;i++)
    {
        if( current->state.feild[5-i][2+i] == 'o' &&
            current->state.feild[4-i][3+i] == 'o' &&
            current->state.feild[3-i][4+i] == 'o' &&
            current->state.feild[2-i][5+i] == 'o' )
        {
            return true;
        }
    }
    if( current->state.feild[3][0] == 'o' &&
        current->state.feild[2][1] == 'o' &&
        current->state.feild[1][2] == 'o' &&
        current->state.feild[0][3] == 'o' )
    {
        return true;
    }
    if( current->state.feild[5][3] == 'o' &&
        current->state.feild[4][4] == 'o' &&
        current->state.feild[3][5] == 'o' &&
        current->state.feild[2][6] == 'o' )
    {
        return true;
    }
    return false;
}

